;(function() {
    angular.module('warehouseTerminal')
        .controller('loginController', LoginController);

    LoginController.$inject = ['$scope', '$location', 'terminalSession', 'terminalAuthService'];
    function LoginController(scope, $location, terminalSession, terminalAuthService) {
        var vm = scope;

        vm.init = init;
        vm.onSubmit = onSubmit;
        vm.imei = null;

        function init() {
            if (terminalSession.isAuthorized()) {
                $location.path('/admin');
            }
        }

        function onSubmit() {
            console.log('logging....');
            terminalAuthService.loginDevice({imei: vm.imei})
                .then(function(data) {
                    if (data.storageId > 0) {
                        $location.path('/');
                    } else {
                        $location.path('/storage');
                    }
                }, function(data) {
                    console.log('error login');
                });
        }
    }
})();