;(function() {

    angular
        .module('warehouseTerminal')
        .constant('AUTH_API', {
            auth: '/auth',
            loginDevice: '/auth/device',
            logoutDevice: '/auth/logout'
        })
        .factory('terminalAuthService', TerminalAuthService);

    TerminalAuthService.$inject = ['$q', 'terminalSession', 'AUTH_API', 'warehouseApi'];
    function TerminalAuthService($q, terminalSession, AUTH_API, warehouseApi) {
        return {
            authorize: authorize,
            loginDevice: loginDevice,
            logoutDevice: logoutDevice
        };

        function authorize() {
            var def = $q.defer();

            warehouseApi.get(AUTH_API.auth)
                .success(function(data) {
                    if (data.deviceId > 0) {
                        def.resolve(data);
                    } else {
                        def.reject();
                    }
                })
                .error(function() {
                    def.reject();
                });

            return def.promise;
        }

        function loginDevice(data) {
            var def = $q.defer();

            warehouseApi.post(AUTH_API.loginDevice, data)
                .success(function(data) {
                    if (data.deviceId > 0) {
                        terminalSession.loginSuccess(data);
                        def.resolve(data);
                    } else {
                        terminalSession.loginFailed();
                        def.reject();
                    }
                })
                .error(function() {
                    def.reject();
                });

            return def.promise;
        }

        function logoutDevice() {
            var def = $q.defer();

            warehouseApi.post(AUTH_API.logoutDevice)
                .success(function(data) {
                    if (data.deviceId == 0) {
                        terminalSession.logoutSuccess(data);
                        def.resolve(data);
                    } else {
                        terminalSession.logoutFailed();
                        def.reject();
                    }
                })
                .error(function() {
                    def.reject();
                });

            return def.promise;
        }
    }

})();