;(function() {
    angular.module('warehouseTerminal')
        .controller('logoutController', LogoutController);

    LogoutController.$inject = ['$scope', '$location', 'terminalSession', 'terminalAuthService'];
    function LogoutController(scope, $location, terminalSession, terminalAuthService) {
        var vm = scope;

        vm.init = init;

        function init() {
            terminalAuthService.logoutDevice();
        }
    }
})();