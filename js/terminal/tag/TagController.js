;(function() {
    angular.module('warehouseTerminal')
        .controller('tagController', TagController);

    TagController.$inject = ['$scope', 'tagService', '$routeParams'];
    function TagController(scope, tagService, $routeParams) {
        var vm = scope;
        vm.init = init;
        vm.tag = {};

        function init() {
            tagService.getTag($routeParams.tagId)
                .success(function(response) {
                    vm.tag = response.data;
                });
        }
    }
})();