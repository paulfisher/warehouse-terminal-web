;(function() {
    angular.module('warehouseTerminal')
        .controller('addTagController', AddTagController);

    AddTagController.$inject = ['$scope', 'tagService', '$routeParams', '$location', 'productCategoryService', 'productService'];
    function AddTagController(scope, tagService, $routeParams, $location, productCategoryService, productService) {
        var vm = scope;
        vm.init = init;
        vm.save = save;

        vm.tag = {
            rfId: $routeParams.rfId
        };
        vm.categories = [];
        vm.category = null;

        function init() {
            productCategoryService.findProductCategories()
                .success(function(response) {
                    vm.categories = response.data.categories;
                });
        }

        function save() {
            if (vm.category) {
                tagService.createTag(vm.tag)
                    .success(function(response) {
                        var tag = response.data;
                        bindTagCategory(tag, vm.category);
                    });
            }

        }

        function bindTagCategory(tag, category) {
            productService.createProduct({
                tagId: tag.id,
                category: {
                    id: category.id
                }
            }).success(function(response) {
                    $location.path('/tag/' + response.data.tagId);
                });
        }
    }
})();