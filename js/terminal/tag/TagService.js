;(function () {

    angular.module('warehouseTerminal')
        .constant('TAG_API',
        {
            findByRfIds: '/tag/findByRfIds',
            get: '/tag/get',
            create: '/tag/create',
            edit: '/tag/edit',
            delete: '/tag/delete'
        })
        .factory('tagService', TagService);

    TagService.$inject = ['TAG_API', 'warehouseApi'];
    function TagService(tagApi, warehouseApi) {
        return {
            findByRfIds: findByRfIds,
            getTag: getTag,
            createTag: createTag,
            editTag: editTag,
            deleteTag: deleteTag
        };

        function findByRfIds(tagList) {
            return warehouseApi.post(tagApi.findByRfIds, {rfIds: tagList});
        }

        function getTag(tagId) {
            return warehouseApi.post(tagApi.get, {tagInfo: {id : tagId}});
        }

        function createTag(tagInfo) {
            return warehouseApi.post(tagApi.create, {tagInfo: tagInfo})
        }

        function editTag(tagInfo) {
            return warehouseApi.post(tagApi.edit, {tagInfo: tagInfo})
        }

        function deleteTag(tagId) {
            return warehouseApi.post(tagApi.delete, {tagInfo: {id: tagId}})
        }
    }

})();