;(function () {

angular.module('warehouseTerminal')
    .constant('PRODUCT_CATEGORY_API',
        {
            find: '/product/category/find',
            get: '/product/category/get',
            create: '/product/category/create',
            edit: '/product/category/edit',
            delete: '/product/category/delete'
        })
    .factory('productCategoryService', ProductCategoryService);

ProductCategoryService.$inject = ['PRODUCT_CATEGORY_API', 'warehouseApi'];
function ProductCategoryService(productCategoryApi, warehouseApi) {
    return {
        findProductCategories: findProductCategories,
        getProductCategory: getProductCategory,
        createProductCategory: createProductCategory,
        editProductCategory: editProductCategory,
        deleteProductCategory: deleteProductCategory
    };

    function findProductCategories(productCategoryInfo) {
        return warehouseApi.post(productCategoryApi.find, {productCategoryInfo: productCategoryInfo});
    }

    function getProductCategory(productCategoryId) {
        return warehouseApi.post(productCategoryApi.get, {productCategoryInfo: {id : productCategoryId}});
    }

    function createProductCategory(productCategoryInfo) {
        return warehouseApi.post(productCategoryApi.create, {productCategoryInfo: productCategoryInfo})
    }

    function editProductCategory(productCategoryInfo) {
        return warehouseApi.post(productCategoryApi.edit, {productCategoryInfo: productCategoryInfo})
    }

    function deleteProductCategory(productCategoryId) {
        return warehouseApi.post(productCategoryApi.delete, {productCategoryInfo: {id: productCategoryId}})
    }
}

})();