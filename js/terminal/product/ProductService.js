;(function () {

angular.module('warehouseTerminal')
    .constant('PRODUCT_API',
        {
            find: '/product/find',
            get: '/product/get',
            create: '/product/create',
            edit: '/product/edit',
            delete: '/product/delete'
        })
    .factory('productService', ProductService);

ProductService.$inject = ['PRODUCT_API', 'warehouseApi'];
function ProductService(productApi, warehouseApi) {
    return {
        findProducts: findProducts,
        getProduct: getProduct,
        createProduct: createProduct,
        editProduct: editProduct,
        deleteProduct: deleteProduct
    };

    function findProducts(productInfo) {
        return warehouseApi.post(productApi.find, {productInfo: productInfo});
    }

    function getProduct(productId) {
        return warehouseApi.post(productApi.get, {productInfo: {id : productId}});
    }

    function createProduct(productInfo) {
        return warehouseApi.post(productApi.create, {productInfo: productInfo})
    }

    function editProduct(productInfo) {
        return warehouseApi.post(productApi.edit, {productInfo: productInfo})
    }

    function deleteProduct(productId) {
        return warehouseApi.post(productApi.delete, {productInfo: {id: productId}})
    }
}

})();