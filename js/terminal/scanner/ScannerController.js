;(function() {
    angular.module('warehouseTerminal')
        .controller('scannerController', ScannerController);

    ScannerController.$inject = ['$scope', 'tagService'];
    function ScannerController(scope, tagService) {
        var vm = scope;
        vm.init = init;
        vm.scan = scan;
        vm.addTag = addTag;
        vm.tags = [];

        function init() {
            addTag();
        }

        function scan() {
            var tagList = [];
            angular.forEach(vm.tags, function(tag) {
                tag.reset();
                tagList.push(tag.rfId);
            });
            tagService.findByRfIds(tagList)
                .success(function(response) {
                    handleScanResult(response.data.foundTags.tags)
                });
        }

        function addTag() {
            vm.tags.push({
                rfId: "",
                tagId: 0,
                scanned: false,
                reset: function() {
                    this.scanned = false;
                    this.tagId = 0;
                }
            });
        }

        function handleScanResult(tags) {
            angular.forEach(vm.tags, function(tag) {
                tag.scanned = true;
            });
            angular.forEach(tags, function(tag) {
                var model = findModel(tag.rfId);
                if (model) {
                    model.tagId = tag.id;
                }
            });
        }

        function findModel(rfId) {
            for (var key in vm.tags) {
                if (vm.tags.hasOwnProperty(key)) {
                    if (vm.tags[key].rfId == rfId) {
                        return vm.tags[key];
                    }
                }
            }
            return null;
        }
    }
})();