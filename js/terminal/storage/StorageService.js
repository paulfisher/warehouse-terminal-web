;(function () {

    angular.module('warehouseTerminal')
        .constant('STORAGE_API',
        {
            findStorages: '/storage/find',
            setStorage: '/storage/set'
        })
        .factory('storageService', StorageService);

    StorageService.$inject = ['STORAGE_API', 'warehouseApi'];
    function StorageService(storageApi, warehouseApi) {
        return {
            findStorages: findStorages,
            setStorage: setStorage
        };

        function findStorages() {
            return warehouseApi.post(storageApi.findStorages);
        }

        function setStorage(storageInfo) {
            return warehouseApi.post(storageApi.setStorage, {info: storageInfo});
        }
    }

})();