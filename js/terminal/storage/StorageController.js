;(function() {
    angular.module('warehouseTerminal')
        .controller('storageController', StorageController);

    StorageController.$inject = ['$scope', 'storageService', '$location'];
    function StorageController(scope, storageService, $location) {
        var vm = scope;
        vm.init = init;
        vm.setStorage = setStorage;

        vm.storages = [];
        vm.storage = null;

        function init() {
            storageService.findStorages()
                .success(function(response) {
                    vm.storages = response.data.storages;
                });
        }

        function setStorage() {
            if (vm.storage) {
                storageService.setStorage({
                    id: vm.storage
                }).success(function(response) {
                    console.log(response);
                    $location.path('/');
                });
            }
        }
    }
})();