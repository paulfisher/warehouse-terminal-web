;(function() {

    angular
        .module('warehouseTerminal')
        .controller('menuController', MenuController);

    MenuController.$inject = ['$scope', 'terminalSession', '$location', 'AUTH_EVENTS', '$rootScope', 'terminalAuthService'];
    function MenuController(scope, terminalSession, $location, AUTH_EVENTS, $rootScope, terminalAuthService) {
        var vm = scope;

        vm.authorized = terminalSession.isAuthorized();

        $rootScope.$on(AUTH_EVENTS.loginSuccess, updateAuthorized);
        $rootScope.$on(AUTH_EVENTS.loginFailed, updateAuthorized);
        $rootScope.$on(AUTH_EVENTS.logoutSuccess, updateAuthorized);
        $rootScope.$on(AUTH_EVENTS.sessionTimeout, updateAuthorized);
        $rootScope.$on(AUTH_EVENTS.notAuthenticated, updateAuthorized);
        $rootScope.$on(AUTH_EVENTS.notAuthorized, updateAuthorized);

        function updateAuthorized() {
            vm.authorized = terminalSession.isAuthorized();
            if (!vm.authorized) {
                //check one more time to be sure
                terminalAuthService.authorize()
                    .then(function(data) {
                        console.log('authorized');
                        terminalSession.authSuccess(data);
                    }, function() {
                        console.log('not authorized');
                        $location.path('/login');
                    });
            }
        }
    }

})();