angular
    .module('warehouseTerminal')
    .factory('terminalRequestInterceptor', TerminalRequestInterceptor)
    .constant('warehouseConfig', {
        apiBaseUrl: 'http://localhost:8080'
    })
    .config(WarehouseConfig);

var isAuthorized = function($q, terminalSession) {
    if (!terminalSession.isAuthorized()) {
        terminalSession.authFailed();
    }
    return terminalSession.isAuthorized();
};

WarehouseConfig.$inject = ['$routeProvider', '$httpProvider'];
function WarehouseConfig($routeProvider, $httpProvider) {
    $httpProvider.interceptors.push('terminalRequestInterceptor');
    $httpProvider.defaults.withCredentials = true;

    $routeProvider
        .when('/', {
            templateUrl : 'pages/index.html',
            controller  : 'indexController',
            resolve: { authorized: isAuthorized }
        })
        .when('/login', {
            templateUrl : 'pages/login.html',
            controller  : 'loginController'
        })
        .when('/logout', {
            templateUrl : 'pages/logout.html',
            controller  : 'logoutController',
            resolve: { authorized: isAuthorized }
        })
        .when('/storage', {
            templateUrl : 'pages/storage.html',
            controller : 'storageController',
            resolve: { authorized: isAuthorized }
        })
        .when('/scanner', {
            templateUrl : 'pages/scanner.html',
            controller : 'scannerController',
            resolve: { authorized: isAuthorized }
        })
        .when('/tag/:tagId', {
            templateUrl : 'pages/tag.html',
            controller : 'tagController',
            resolve: { authorized: isAuthorized }
        })
        .when('/tag/add/:rfId', {
            templateUrl : 'pages/addTag.html',
            controller : 'addTagController',
            resolve: { authorized: isAuthorized }
        })

        .otherwise({
            redirectTo: '/'
        });
}

TerminalRequestInterceptor.$inject = ['$q', 'terminalSession'];
function TerminalRequestInterceptor($q, terminalSession) {
    return {
        responseError: function(error) {
            terminalSession.authFailed(error);
            return $q.reject(error);
        }
    };
}