;(function() {
    angular
        .module('warehouseTerminal')
        .constant('AUTH_EVENTS', {
            loginSuccess: 'auth-login-success',
            loginFailed: 'auth-login-failed',
            logoutSuccess: 'auth-logout-success',
            sessionTimeout: 'auth-session-timeout',
            notAuthenticated: 'auth-not-authenticated',
            notAuthorized: 'auth-not-authorized'
        })
        .service('terminalSession', TerminalSession);

    TerminalSession.$inject = ['AUTH_EVENTS', '$rootScope', '$sessionStorage'];
    function TerminalSession(AUTH_EVENTS, $rootScope, $sessionStorage) {
        return {
            loginSuccess : loginSuccess,
            loginFailed : loginFailed,
            authSuccess : authSuccess,
            authFailed : authFailed,
            isAuthorized : isAuthorized,
            logoutSuccess : logoutSuccess,
            logoutFailed : logoutFailed
        };

        function loginSuccess(data) {
            this.data = data;
            $sessionStorage.authorized = true;
            $rootScope.$broadcast(AUTH_EVENTS.loginSuccess, data);
        }

        function loginFailed() {
            $sessionStorage.authorized = false;
            $rootScope.$broadcast(AUTH_EVENTS.loginFailed, {});
        }

        function authSuccess(data) {
            this.data = data;
            $sessionStorage.authorized = true;
            $rootScope.$broadcast(AUTH_EVENTS.loginSuccess, {});
        }

        function authFailed() {
            $sessionStorage.authorized = false;
            $rootScope.$broadcast(AUTH_EVENTS.notAuthenticated, {});
        }

        function isAuthorized() {
            return $sessionStorage.authorized;
        }

        function logoutSuccess(data) {
            this.data = data;
            $sessionStorage.authorized = false;
            $rootScope.$broadcast(AUTH_EVENTS.logoutSuccess, data);
        }

        function logoutFailed(data) {
            this.data = data;
            $rootScope.$broadcast(AUTH_EVENTS.logoutFailed, data);
        }
    }
})();